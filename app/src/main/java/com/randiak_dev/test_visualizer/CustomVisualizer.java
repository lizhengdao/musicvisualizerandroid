package com.randiak_dev.test_visualizer;

import android.media.audiofx.Visualizer;
import android.util.Pair;

public class CustomVisualizer extends Visualizer implements Visualizer.OnDataCaptureListener {
    private static final int DEFAULT_CAPTURE_SIZE = 1024;
    private static final int DEFAULT_SCALING_MODE = Visualizer.SCALING_MODE_AS_PLAYED;
    private static final int DEFAULT_MEASUREMENT_MODE = Visualizer.MEASUREMENT_MODE_PEAK_RMS;

    private OnVisualizeDataListener listener;

    public CustomVisualizer(int audioSession) throws RuntimeException {
        super(audioSession);
        setDataCaptureListener(this, getMaxCaptureRate(),false, true);
        listener = null;
    }

//    public CustomVisualizer() {
//        this.visualizer = new Visualizer(0);
//        visualizer.setDataCaptureListener(this, visualizer.getMaxCaptureRate(),false, true);
//        visualizer.setEnabled(true);
//        listener = null;
//    }

    public void setVisualizeDataListener(OnVisualizeDataListener listener) {
        this.listener = listener;
    }

    @Override
    public void onWaveFormDataCapture(Visualizer visualizer, byte[] waveform, int samplingRate) {

    }

    @Override
    public void onFftDataCapture(Visualizer visualizer, byte[] fft, int samplingRate) {
        onFftCapture(visualizer, fft, samplingRate);
    }

    private void onFftCapture(Visualizer visualizer, byte[] fft, int samplingRate) {
        if (listener != null) {
            listener.onDBsDataCapture(visualizer, calcDBs(visualizer, fft, samplingRate), samplingRate);
        }
    }

    private Pair<Float, Float>[] calcDBs(Visualizer visualizer, byte[] fft, int samplingRate) {
        int n = visualizer.getCaptureSize() / 2 + 1;
        float maxRateHZ = samplingRate / 2 / 1000;
        Pair<Float, Float>[] freqDBs = new Pair[n];
        freqDBs[0] = Pair.create((float) 0, magnitudeToDB(Math.abs(fft[0])));                        // DC
        freqDBs[n - 1] = Pair.create((float) maxRateHZ, magnitudeToDB(Math.abs(fft[1]))); // Nyquist

        for (int i = 1; i < (n - 1); i++) {
            int k = i * 2;
            float freq = (float) (i * maxRateHZ) / (n - 1);
            float real = fft[k];
            float imag = fft[k + 1];
            float db = magnitudeToDB((float) (Math.pow(real, 2) + Math.pow(imag, 2)));
            freqDBs[i] = Pair.create(freq, db);
        }
        return freqDBs;
    }

    private float magnitudeToDB(float magnitude) {
        if (magnitude == 0) {
            return 0;
        } else {
            return 10 * (float) Math.log10(magnitude);
        }
    }

    //    public void setDataCaptureListener(final CustomVisualizer.OnDataCaptureListener listener, int rate,
//                                       boolean waveform, boolean fft, boolean dbs, boolean peek) {
//        super.setDataCaptureListener(new Visualizer.OnDataCaptureListener() {
//            @Override
//            public void onWaveFormDataCapture(Visualizer visualizer, byte[] waveform, int samplingRate) {
//                listener.onWaveFormDataCapture(visualizer, waveform, samplingRate);
//            }
//
//            @Override
//            public void onFftDataCapture(Visualizer visualizer, byte[] fft, int samplingRate) {
//
//            }
//        })
//        this.listener = listener;
//    }

    public interface OnVisualizeDataListener {
        public void onDBsDataCapture(Visualizer visualizer, Pair<Float, Float>[] freqDBs, int samplingRate);
        public void onPeakCapture(Visualizer visualizer, byte[] fft, int samplingRate);
    }
}
