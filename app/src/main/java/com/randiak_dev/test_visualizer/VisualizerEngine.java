package com.randiak_dev.test_visualizer;

import android.media.audiofx.Visualizer;
import android.util.Pair;

public class VisualizerEngine implements Visualizer.OnDataCaptureListener {
    private static VisualizerEngine instance;
    private OnDataCaptureListener listener;

    private static Visualizer visualizer;
    private static final int DEFAULT_CAPTURE_SIZE = 1024;
    private static final int DEFAULT_SCALING_MODE = Visualizer.SCALING_MODE_AS_PLAYED;
    private static final int DEFAULT_MEASUREMENT_MODE = Visualizer.MEASUREMENT_MODE_PEAK_RMS;

    public static synchronized VisualizerEngine getInstance(int audioSession) {
        if (instance == null) {
            instance = new VisualizerEngine(audioSession);
        }
        return instance;
    }

    public VisualizerEngine(int audioSession) {
        visualizer = new Visualizer(audioSession);
        visualizer.setCaptureSize(DEFAULT_CAPTURE_SIZE);
        visualizer.setScalingMode(DEFAULT_SCALING_MODE);
        visualizer.setMeasurementMode(DEFAULT_MEASUREMENT_MODE);
        this.listener = null;
    }

    public void startVisualizer() {
        visualizer.setEnabled(true);
    }

    public void stopVisualizer() {
        visualizer.setEnabled(false);
    }

    public int setCaptureSize(int captureSize) {
        int retValue;
        if (visualizer.getEnabled()) {
            stopVisualizer();
            retValue = visualizer.setCaptureSize(captureSize);
            startVisualizer();
        } else {
            retValue = visualizer.setCaptureSize(captureSize);
        }
        return retValue;
    }

    public int setScalingMode(int scalingMode) {
        int retValue;
        if (visualizer.getEnabled()) {
            stopVisualizer();
            retValue = visualizer.setScalingMode(scalingMode);
            startVisualizer();
        } else {
            retValue = visualizer.setScalingMode(scalingMode);
        }
        return retValue;
    }

    public int setMeasurementMode(int measurementMode) {
        int retValue;
        if (visualizer.getEnabled()) {
            stopVisualizer();
            retValue = visualizer.setMeasurementMode(measurementMode);
            startVisualizer();
        } else {
            retValue = visualizer.setMeasurementMode(measurementMode);
        }
        return retValue;
    }

    public void setOnDataCaptureListener(OnDataCaptureListener listener) {
        this.listener = listener;
    }

    @Override
    public void onWaveFormDataCapture(Visualizer visualizer, byte[] waveform, int samplingRate) {
        if (listener != null) {
            listener.onWaveFormDataCapture(visualizer, waveform, samplingRate);
        }
    }

    @Override
    public void onFftDataCapture(Visualizer visualizer, byte[] fft, int samplingRate) {
        if (listener != null) {
            listener.onFftDataCapture(visualizer, fft, samplingRate);
            listener.onDBDataCapture(visualizer, calcDBs(visualizer, fft, samplingRate), samplingRate);
        }
    }

    public Pair<Float, Float>[] calcDBs(Visualizer visualizer, byte[] fft, int samplingRate) {
        int n = visualizer.getCaptureSize() / 2 + 1;
        Pair<Float, Float>[] freqDBs = new Pair[n];
        freqDBs[0] = Pair.create((float) 0, magnitudeToDB(Math.abs(fft[0])));                        // DC
        freqDBs[n - 1] = Pair.create((float) samplingRate / 2, magnitudeToDB(Math.abs(fft[1]))); // Nyquist

        for (int i = 1; i < (n - 1); i++) {
            int k = i * 2;
            float freq = (float) (k * samplingRate) / n;
            float real = fft[k];
            float imag = fft[k + 1];
            float db = magnitudeToDB((float) (Math.pow(real, 2) + Math.pow(imag, 2)));
            freqDBs[i] = Pair.create(freq, db);
        }
        return freqDBs;
    }

    private float magnitudeToDB(float freq) {
        if (freq == 0) {
            return 0;
        } else {
            return 10 * (float) Math.log10(freq);
        }
    }
//
//    private Pair<Float, Float> getPeek(Visualizer visualizer, byte[] fft, int samplingRate) {
//        if (visualizer.getEnabled() &&
//                visualizer.getMeasurementMode() == Visualizer.MEASUREMENT_MODE_PEAK_RMS) {
//            Visualizer.MeasurementPeakRms mPeakRms = new Visualizer.MeasurementPeakRms();
//            visualizer.getMeasurementPeakRms(mPeakRms);
//            float peek = mPeakRms.mPeak;
//        }
//        Pair<Float, Float> peek = Pair.create();
//    }

    public interface OnDataCaptureListener {
        public void onWaveFormDataCapture(Visualizer visualizer, byte[] waveform, int samplingRate);
        public void onFftDataCapture(Visualizer visualizer, byte[] fft, int samplingRate);
        public void onDBDataCapture(Visualizer visualizer, Pair<Float, Float>[] freqDBs, int samplingRate);
        public void onPeakCapture(Visualizer visualizer, byte[] fft, int samplingRate);
    }
}
